import java.util.Scanner;

public class HotelManagementApp {
    public static void main(String[] args) {
        Hotel hotel = new Hotel();
        Scanner scanner = new Scanner(System.in);

      System.out.println("   ___ ___     _____    ___________  _______.    ______            ____.    ________  __________");
      System.out.println(" //   |   \\  //     \\  \\__    ___//  \\_  ___//   |    |            |     | \\   ___ \\   \\____   \\");
      System.out.println("//    ~    \\ |   |   |    |    |      |  __)_    |    |            |     | //  // \\//  |   |   // ");
      System.out.println("\\    Y    // |   |   |    |    |      |      \\   |    |___     //\\__|    | \\  \\____    |   |    \\ ");
      System.out.println(" \\___|___//  \\______/     /____|      |_______// |_______\\     \\_________|  \\______// |________// ");

      System.out.println("\nGestione Hotel Jacopozzo, Ciccio & Bovetto\n");

      
        while (true) {

            System.out.println("\n1. Visualizza stanze disponibili");
            System.out.println("2. Visualizza prenotazioni");
            System.out.println("3. Effettua una prenotazione");
            System.out.println("4. Esci");
            System.out.print("Scelta: ");

            int choice = scanner.nextInt();
            scanner.nextLine(); // Consuma il newline

            switch (choice) {
                case 1:
                    hotel.displayAvailableRooms();
                    break;
                case 2:
                    hotel.displayReservations();
                    break;
                case 3:
                    hotel.makeReservation(scanner);
                    break;
                case 4:
                    System.out.println("\nGrazie per aver utilizzato l'applicazione!");
                    System.exit(0);
                default:
                    System.out.println("Scelta non valida. Riprova.");
            }
        }
    }
}
