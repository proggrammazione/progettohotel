public class Reservation {
    private Room room;
    private Guest guest;
    private double amountPaid;
    private int nights;

    public Reservation(Room room, Guest guest, double amountPaid, int nights) {
        this.room = room;
        this.guest = guest;
        this.amountPaid = amountPaid;
        this.nights = nights;
    }

    public String toString() {
        return "Stanza " + room.getRoomNumber() + " - Cliente: " + guest.getName() + " - Importo Pagato: $" + amountPaid + " - Permanenza: " + nights + " notti";
    }
}
